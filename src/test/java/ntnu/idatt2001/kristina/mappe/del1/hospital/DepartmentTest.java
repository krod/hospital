package ntnu.idatt2001.kristina.mappe.del1.hospital;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class DepartmentTest {
    @Nested
    class NewEmployee {
        @Test
        public void employeeIsAdded() {
            Department DP = new Department("Childrens DP");
            Employee employee = new Employee("Maria","Vigre", "01017522222");

            DP.addEmployee(employee);

            assertTrue(DP.getEmployees().containsValue(employee));
        }

        @Test
        public void employeeIsReplaced() {
            Department DP = new Department("Childrens DP");
            Employee employee = new Employee("Maria","Vigre", "01017522222");
            Employee employeeNewName = new Employee("Maria Vigre","Wiik", "01017522222");

            DP.addEmployee(employee);
            DP.addEmployee(employeeNewName);

            assertTrue(!DP.getEmployees().containsValue(employee) && DP.getEmployees().containsValue(employeeNewName));
        }
    }

    @Nested
    class NewPatient {
        @Test
        public void patientIsAdded() {
            Department DP = new Department("Childrens DP");
            Patient patient = new Patient("Olav","Vigre", "01011622122");

            DP.addPatient(patient);

            assertTrue(DP.getPatients().containsValue(patient));
        }

        @Test
        public void patientIsReplaced() {
            Department DP = new Department("Childrens DP");
            Patient patient = new Patient("Olav","Vigre", "01011622122");
            Patient patientNewName = new Patient("Olav Wiik","Vigre", "01011622122");

            DP.addPatient(patient);
            DP.addPatient(patientNewName);

            assertTrue(!DP.getPatients().containsValue(patient) && DP.getPatients().containsValue(patientNewName));
        }
    }

    @Nested
    class RemovePerson {
        @Test
        @DisplayName("Remove registered employee")
        public void removeRegisteredEmployee() {
            Department DP = new Department("Childrens DP");
            Employee employee = new Employee("Maria","Vigre", "01017522222");

            DP.addEmployee(employee);
            assertTrue(DP.getEmployees().containsValue(employee));

            try{
                DP.remove(employee);
            }
            catch (RemoveException e) {
                assertThrows(RemoveException.class, () -> DP.remove(employee));
            }
            assertFalse(DP.getEmployees().containsValue(employee));
        }

        @Test
        @DisplayName("Remove non registered employee")
        public void removeNonRegisteredEmployee() {
            Department DP = new Department("Childrens DP");
            Employee employee = new Employee("Maria","Vigre", "01017522222");

            assertFalse(DP.getEmployees().containsValue(employee));

            try{
                DP.remove(employee);
            }
            catch (RemoveException e) {
                assertThrows(RemoveException.class, () -> DP.remove(employee));
            }
            assertFalse(DP.getEmployees().containsValue(employee));
        }

        @Test
        @DisplayName("Remove registered patient")
        public void removeRegisteredPatient() {
            Department DP = new Department("Childrens DP");
            Patient patient = new Patient("Olav","Vigre", "01011622122");

            DP.addPatient(patient);
            assertTrue(DP.getPatients().containsValue(patient));

            try{
                DP.remove(patient);
            }
            catch (RemoveException e) {
                assertThrows(RemoveException.class, () -> DP.remove(patient));
            }
            assertFalse(DP.getPatients().containsValue(patient));
        }

        @Test
        @DisplayName("Remove non registered patient")
        public void removeNonRegisteredPatient() {
            Department DP = new Department("Childrens DP");
            Patient patient = new Patient("Olav","Vigre", "01011622122");

            assertFalse(DP.getPatients().containsValue(patient));

            try{
                DP.remove(patient);
            }
            catch (RemoveException e) {
                assertThrows(RemoveException.class, () -> DP.remove(patient));
            }
            assertFalse(DP.getPatients().containsValue(patient));
        }
    }

}