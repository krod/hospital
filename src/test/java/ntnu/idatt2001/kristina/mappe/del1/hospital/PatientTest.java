package ntnu.idatt2001.kristina.mappe.del1.hospital;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class PatientTest {
    @Test
    public void diagnosisIsSet() {
        Patient patient = new Patient("Sverre","Vigre", "01011622122");
        String diagnosis = "Leumeki";
        patient.setDiagnosis(diagnosis);
        assertEquals(diagnosis, patient.getDiagnosis());
    }
}
