package ntnu.idatt2001.kristina.mappe.del1.hospital;

import java.util.HashMap;
import java.util.Objects;

public class Department {
    private String departmentName;
    private HashMap<String,Employee> employees;
    private HashMap<String, Patient> patients;

    /**
     * Represents a department of a hospital. Holds a list of the
     * employees of the department and a list of the patients currently
     * admitted.
     */
    public Department(String departmentName) {
        this.departmentName = departmentName;
        employees = new HashMap<String, Employee>();
        patients = new HashMap<String, Patient>();
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public HashMap<String, Employee> getEmployees() {
        return employees;
    }

    /**
     * Adds an employee to the list of employees at the department. If
     * an employee object with the equal social security number already
     * exists in the list, this will be replaced by the new employee
     * object.
     * @param employee The employee that will be added.
     */
    public void addEmployee(Employee employee) {
        employees.put(employee.getSocialSecurityNumber(), employee);
    }

    public HashMap<String, Patient> getPatients() {
        return patients;
    }

    /**
     * Adds a patient to the list of patients at the department. If
     * a patient object with the equal social security number already
     * exists in the list, this will be replaced by the new patient
     * object.
     */
    public void addPatient(Patient patient) {
        patients.put(patient.getSocialSecurityNumber(),patient);
    }

    /**
     * Removes a person from the department.
     * @param person An employee or patient
     * @throws RemoveException If the person is not registered as an employee or patient, it throws
     * an exeption.
     */
    public void remove(Person person) throws RemoveException{
        if (!employees.containsValue(person) && !patients.containsValue(person)) {
            throw new RemoveException("The person is not registered as an employee or patient");
        }
        else if (person instanceof Employee) {
            employees.remove(person.getSocialSecurityNumber());
        }
        else {
            patients.remove(person.getSocialSecurityNumber());
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(departmentName, employees, patients);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Department that = (Department) o;
        return departmentName.equals(that.departmentName);
    }

    @Override
    public String toString() {
        return "\n" + departmentName.toUpperCase() +
                "\nEmployees:\n" + employees.toString().substring(1,employees.toString().length()-1) +
                "\nCurrent patients:\n" + patients.toString().substring(1,patients.toString().length()-1);

    }
}
