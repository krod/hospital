package ntnu.idatt2001.kristina.mappe.del1.hospital;

/**
 * An exception triggered in context of remove(Person person) in the Department class.
 */
public class RemoveException extends Exception {
    RemoveException(String message) {
        super(message);
    }
}