package ntnu.idatt2001.kristina.mappe.del1.hospital;

public interface Diagnosable {
    void setDiagnosis(String diagnosis);
}
