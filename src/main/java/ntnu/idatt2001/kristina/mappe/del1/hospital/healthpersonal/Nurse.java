package ntnu.idatt2001.kristina.mappe.del1.hospital.healthpersonal;

import ntnu.idatt2001.kristina.mappe.del1.hospital.Employee;

public class Nurse extends Employee {
    public Nurse(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    @Override
    public String toString() {
        return "\nNURSE" +
                "\nName: " + getFullName() +
                "\nSocial security number: " + getSocialSecurityNumber() + "\n";
    }
}
