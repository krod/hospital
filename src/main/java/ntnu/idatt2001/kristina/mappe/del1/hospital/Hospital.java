package ntnu.idatt2001.kristina.mappe.del1.hospital;

import java.util.HashSet;
import java.util.Objects;

public class Hospital {
    private final String hospitalName;
    private HashSet<Department> departments;

    public Hospital(String hospitalName) {
        this.hospitalName = hospitalName;
        this.departments = new HashSet<Department>();
    }

    public String getHospitalName() {
        return hospitalName;
    }

    public HashSet<Department> getDepartments() {
        return departments;
    }

    /**
     * Adds a department to the hospitals list of departments.
     */
    public void addDepartment(Department department) {
        departments.add(department);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Hospital hospital = (Hospital) o;
        return hospitalName.equals(hospital.hospitalName) && departments.equals(hospital.departments);
    }

    @Override
    public int hashCode() {
        return Objects.hash(hospitalName, departments);
    }

    @Override
    public String toString() {
        return "HOSPITAL" +
                "\nName: " + hospitalName +
                "\nDepartments: " + departments.toString().substring(1,departments.toString().length()-1);
    }
}
