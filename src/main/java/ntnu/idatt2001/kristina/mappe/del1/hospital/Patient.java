package ntnu.idatt2001.kristina.mappe.del1.hospital;

/**
 * A patient can be diagnosed by a doctor.
 */
public class Patient extends Person implements Diagnosable {
    private String diagnosis = "";

    protected Patient(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    protected String getDiagnosis() {
        return diagnosis;
    }

    /**
     * Updates the patient object variable "diagnosis" from an empty
     * string or the previous diagnosis.
     */
    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    @Override
    public String toString() {
        return "\nPATIENT" +
                "\nName: " + getFullName() +
                "\nSocial security number: " + getSocialSecurityNumber() +
                "\nDiagnosis: " + diagnosis + "\n\n";
    }
}
