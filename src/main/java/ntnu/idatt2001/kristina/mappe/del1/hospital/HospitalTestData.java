package ntnu.idatt2001.kristina.mappe.del1.hospital;
import ntnu.idatt2001.kristina.mappe.del1.hospital.healthpersonal.Nurse;
import ntnu.idatt2001.kristina.mappe.del1.hospital.healthpersonal.doctor.GeneralPractitioner;
import ntnu.idatt2001.kristina.mappe.del1.hospital.healthpersonal.doctor.Surgeon;
public final class HospitalTestData {
    private HospitalTestData() {
        // Not called
    }

    public static void fillRegisterWithTestData(final Hospital hospital) {
// Add some departments
        Department emergency = new Department("Akutten");
        emergency.addEmployee(new Employee("Odd Even", "Primtallet", "1122334444"));
        emergency.addEmployee(new Employee("Huppasahn", "DelFinito", "2233445555"));
        emergency.addEmployee(new Employee("Rigmor", "Mortis", "33445566666"));
        emergency.addEmployee(new GeneralPractitioner("Inco", "Gnito", "44556677777"));
        emergency.addEmployee(new Surgeon("Inco", "Gnito", "55667788888"));
        emergency.addEmployee(new Nurse("Nina", "Teknologi", "66778899999"));
        emergency.addEmployee(new Nurse("Ove", "Ralt", "77889900000"));
        emergency.addPatient(new Patient("Inga", "Lykke", "88990011111"));
        emergency.addPatient(new Patient("Ulrik", "Smål", "99001122222"));
        hospital.addDepartment(emergency);
        Department childrenPolyclinic = new Department("Barn poliklinikk");
        childrenPolyclinic.addEmployee(new Employee("Salti", "Kaffen", "00112233333"));
        childrenPolyclinic.addEmployee(new Employee("Nidel V.", "Elvefølger", "01010122222"));
        childrenPolyclinic.addEmployee(new Employee("Anton", "Nym", "02020233333"));
        childrenPolyclinic.addEmployee(new GeneralPractitioner("Gene", "Sis", "03030344444"));
        childrenPolyclinic.addEmployee(new Surgeon("Nanna", "Na", "05050566666"));
        childrenPolyclinic.addEmployee(new Nurse("Nora", "Toriet", "06060677777"));
        childrenPolyclinic.addPatient(new Patient("Hans", "Omvar", "07070788888"));
        childrenPolyclinic.addPatient(new Patient("Laila", "La", "08080899999"));
        childrenPolyclinic.addPatient(new Patient("Jøran", "Drebli", "09090900000"));
        hospital.addDepartment(childrenPolyclinic);
    }
}