package ntnu.idatt2001.kristina.mappe.del1.hospital;

public class Employee extends Person {
    public Employee(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    @Override
    public String toString() {
        return "\nEMPLOYEE" +
                "\nName: " + getFullName() +
                "\nSocial security number: " + getSocialSecurityNumber() +"\n\n";
    }
}