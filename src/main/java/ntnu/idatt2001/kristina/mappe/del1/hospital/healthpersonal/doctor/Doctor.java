package ntnu.idatt2001.kristina.mappe.del1.hospital.healthpersonal.doctor;

import ntnu.idatt2001.kristina.mappe.del1.hospital.Employee;
import ntnu.idatt2001.kristina.mappe.del1.hospital.Patient;

/**
 * Only doctors can diagnose a patient.
 */
public abstract class Doctor extends Employee {
    protected Doctor(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    public abstract void setDiagnosis(Patient patient, String diagnosis);
}
