package ntnu.idatt2001.kristina.mappe.del1.hospital;


public class HospitalClient {
    public static void main(String[] args) {

        Hospital StOlavs = new Hospital("St. Olavs Hospital");
        HospitalTestData.fillRegisterWithTestData(StOlavs);

        //Remove employee
        for(Department department : StOlavs.getDepartments()) {
            Employee employee = department.getEmployees().get("00112233333");
            int dpPreSize = department.getEmployees().size();
            try {
                department.remove(employee);
            }
            catch (RemoveException e) {
                System.out.println("Exeption: " + e.getMessage() + " at " + department.getDepartmentName() + ".");
            }
            if (dpPreSize > department.getEmployees().size()) {
                System.out.println(employee.getFullName() + " was removed from " + department.getDepartmentName());
                break;
            }
        }


        //Remove non-existent patient
        Patient nonExistentPatient = new Patient("Matteus", "Valde", "020302203020");
        for(Department department : StOlavs.getDepartments()) {
            try {
                department.remove(nonExistentPatient);
            }
            catch (RemoveException e) {
                System.out.println("Exeption: " + e.getMessage() + " at " + department.getDepartmentName() + ".");
            }
        }

    }
}
